require 'CSV' #ruby csv parser
#Parse csv files
csv_confirmed = CSV.read("ncov_confirmed.csv")
csv_deaths = CSV.read("ncov_deaths.csv")
csv_recovered = CSV.read("ncov_recovered.csv")
#Looping over the two dimensional array
=begin
Loop takes one of the file as conditional because all of the files have
same numbers of elements
=end
#File obj
output = File.open("Output.txt", "w+")
date_start = 5
while(date_start < csv_confirmed[0].length)
	state = 1
	#Prints the date first
	output.puts "------#{csv_confirmed[0][date_start]}------"
	while(state < csv_confirmed.length)
		#Date and time
		output.write csv_confirmed[0][date_start].to_s + ", "
		#State/Province name prints ntg if nil
		unless(csv_confirmed[state][0].eql?(nil))
			output.write csv_confirmed[state][0].to_s + ", "
		else
			output.write ""
		end
		#Country
		output.write csv_confirmed[state][1].to_s + ", "
		#Number of confirmed print the number if number!=nil else 0
		unless(csv_confirmed[state][date_start].eql?(nil))
			output.write csv_confirmed[state][date_start].to_s + ", "
		else
			output.write "0, "
		end
		#Number of deaths
		unless(csv_deaths[state][date_start].eql?(nil))
			output.write csv_deaths[state][date_start].to_s + ", "
		else
			output.write "0, "
		end
		#Number of recovered
		unless(csv_recovered[state][date_start].eql?(nil))
			output.write csv_recovered[state][date_start].to_s + "\n"
		else
			output.write "0\n"
		end
		state += 1
	end
	date_start += 1
end
#Closing file obj
output.close()